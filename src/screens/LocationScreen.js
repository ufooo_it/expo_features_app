import React, { useState, useEffect } from "react";
import { Button, View, Text, Dimensions } from "react-native";
import * as Location from "expo-location";
import MapView, { Marker } from "react-native-maps";
const LocationSceen = () => {
  const [location, setLocation] = useState(null);

  const getLocationHandler = async () => {
    let { status } = await Location.requestPermissionsAsync();
    if (status !== "granted") {
      alert("Permission to access location was denied");
    }

    let locationInfo = await Location.getCurrentPositionAsync({});
    setLocation(locationInfo.coords);
    console.log(locationInfo);
  };

  return (
    <View style={{ padding: 10 }}>
      <MapView
        region={
          location
            ? {
                latitude: location.latitude,
                longitude: location.longitude,
                latitudeDelta: 0.1,
                longitudeDelta: 0.1,
              }
            : null
        }
        style={{
          height: Dimensions.get("window").width - 20,
          width: Dimensions.get("window").width - 20,
          marginBottom: 10
        }}
      >
        {location ? (
          <Marker coordinate={location} title="You" description="You are here;" />
        ) : null}
      </MapView>
      <Button title="Get Location" onPress={getLocationHandler} />
    </View>
  );
};

export default LocationSceen;
